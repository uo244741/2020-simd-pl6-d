/*
 * Main.cpp
 *
 * 
 *  Created on: Fall 2020
 *PL6D

 */
 
#include <stdio.h>
#include <immintrin.h>
#include <math.h>
#define cimg_display  1
#include <CImg.h>
#define NTIMES 20
using namespace cimg_library;
#define ITEMS_PER_PACKET (sizeof(__m256)/sizeof(float))

// Data type for image components
// FIXME: Change this type according to your group assignment
typedef float data_t;

const char* SOURCE_IMG      = "bailarina.bmp";
const char* DESTINATION_IMG = "bailarina2.bmp";


int main() {
	// // Open file and object initialization
	CImg<float> A(SOURCE_IMG); // Image A
	int width, height; // Width and height of the image

	int nComp; // Number of image components


	/***************************************************
	 * TODO: Variables initialization.
	 *   - Prepare variables for the algorithm
	 *   - This is not included in the benchmark time
	 */

	A.display(); // Displays the source image
	width  = A.width(); // Getting information from the source image
	height = A.height();
	nComp  = A.spectrum(); // source image number of components
				// Common values for spectrum (number of image components):
				//  B&W images = 1
				//	Normal color images = 3 (RGB)
				//  Special color images = 4 (RGB and alpha/transparency channel)

	int npixels = width * height;
	//Image A
	float *pRsrc, *pGsrc, *pBsrc; // Pointers to the R, G and B components
	// Allocate memory space for destination image components

	//Image C (Salida)
	float *pRnewC, *pGnewC, *pBnewC;
	float *pdstImageC; // Pointer to the new image pixels
	int nPackets = (npixels* sizeof(float)/sizeof(__m256));
	__m256 vRa,vGa,vBa;
	pdstImageC = (float *)_mm_malloc(sizeof(__m256) * nPackets*nComp, sizeof(__m256));
	if (pdstImageC == NULL) {
		perror("Allocating destination image");
		exit(-2);
	}

	pRsrc = A.data(); // pRcomp points to the R component
	pGsrc = pRsrc + height * width; // pGcomp points to the G component
	pBsrc = pGsrc + height * width; // pBcomp points to B component


	// Pointers to the RGB arrays of the destination image
	pRnewC = pdstImageC;// pRcomp points to the R component
	pGnewC= pRnewC + height * width;// pGcomp points to the G component
	pBnewC= pGnewC + height * width;// pBcomp points to B component
	if ( ((npixels * sizeof(float))%sizeof(__m256)) != 0) {
        nPackets++;
	}

	/***********************************************
	 * TODO: Algorithm start.
	 *   - Measure initial time
	 */


	struct timespec tStart, tEnd;
	double tiempo;
	// clock_gettime(CLOCK_REALTIME,&tStart);
	if(clock_gettime(CLOCK_REALTIME, &tStart)==-1)
	{
		perror("clock_gettime");
		exit(EXIT_FAILURE);
	}
	/************************************************
	 * FIXME: Algorithm.
	 * In this example, the algorithm is a components swap
	 *
	 * TO BE REPLACED BY YOUR ALGORITHM
	 */
	

	float rmin = 255; 
	float gmin = 255; 
	float bmin = 255; 
	float rmax = 0; 
	float gmax = 0; 
	float bmax = 0; 

	for(int w = 0; w<NTIMES;w++){

		for(int i = 0; i<npixels; i++){  
			// 1. bucle que recorra la imagen pixel por pixel, 
			//valor max para rgb 
			// valor minimo rgb 

			float r = *(pRsrc + i);
			float g = *(pGsrc + i);
			float b = *(pBsrc + i);



			if (rmin > r) rmin = r;

			if (gmin > g) gmin = g;

			if (bmin > b) bmin = b;



			if (rmax < r) rmax = r;

			if (gmax < g) gmax = g;

			if (bmax < b) bmax = b;

			
		}	

		__m256 aux, vrmaxmin,v255, vRmin, vgmaxmin, vGmin, vbmaxmin, vBmin; 
		v255 = _mm256_set1_ps(255);
		vrmaxmin = _mm256_set1_ps(rmax - rmin);
		vRmin = _mm256_set1_ps(rmin);
		vgmaxmin = _mm256_set1_ps(gmax - gmin);
		vGmin = _mm256_set1_ps(gmin);
		vbmaxmin = _mm256_set1_ps(bmax - bmin);
		vBmin = _mm256_set1_ps(bmin);
		
		for(int i=0; i<npixels/8; i++){
			
			
			// *(pdest + i)  --> byte x byte 
			vRa = _mm256_loadu_ps((pRsrc + ITEMS_PER_PACKET * i));
			aux = _mm256_sub_ps(vRa,vRmin);
			aux = _mm256_div_ps(aux,vrmaxmin); 
			*(__m256*)(pRnewC + i*8)= _mm256_mul_ps(aux,v255);

			vGa = _mm256_loadu_ps((pGsrc + ITEMS_PER_PACKET * i));
			aux = _mm256_sub_ps(vGa,vGmin);
			aux = _mm256_div_ps(aux,vgmaxmin); 
			*(__m256*)(pGnewC + i*8)= _mm256_mul_ps(aux,v255);

			vBa = _mm256_loadu_ps((pBsrc + ITEMS_PER_PACKET * i));
			aux = _mm256_sub_ps(vBa,vBmin);
			aux = _mm256_div_ps(aux,vbmaxmin); 
			*(__m256*)(pBnewC + i*8)= _mm256_mul_ps(aux,v255);

		}

		for(int i=0; i<npixels; i++){

				if(*(pRnewC + i)>255){
					*(pRnewC + i)=255;
				}
				if(*(pRnewC + i)<0){
					*(pRnewC + i)=0;
				}
				if(*(pGnewC + i)>255){
					*(pGnewC + i)=255;
				}
				if(*(pGnewC + i)<0){
					*(pGnewC + i)=0;
				}
				if(*(pBnewC + i)>255){
					*(pBnewC + i)=255;
				}
				if(*(pBnewC + i)<0){
					*(pBnewC + i)=0;
				}

		}
	}
	/***********************************************
	 * TODO: End of the algorithm.
	 *   - Measure the end time
	 *   - Calculate the elapsed time
	 */
	 


		//clock_gettime(CLOCK_REALTIME,&tEnd);
	if(clock_gettime(CLOCK_REALTIME, &tEnd)==-1)
	{
		perror("clock_gettime");
		exit(EXIT_FAILURE);
	}
	tiempo = (tEnd.tv_sec - tStart.tv_sec );
	tiempo += (tEnd.tv_nsec - tStart.tv_nsec )/1e+9;
	printf("Elapsed time    :%f \n",tiempo);

	// Create a new image object with the calculated pixels
	// In case of normal color images use nComp=3,
	// In case of B/W images use nComp=1.
	CImg<float> dstImageC(pdstImageC, width, height, 1, nComp);

	// Store destination image in disk
	dstImageC.save(DESTINATION_IMG); 

	// Display destination image
	dstImageC.display();
	return 0;
}